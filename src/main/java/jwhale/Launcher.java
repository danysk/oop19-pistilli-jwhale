package jwhale;

import jwhale.view.JWhale;

public final class Launcher {

    private Launcher() { }
    /**
     * Application launcher, JavaFX workaround.
     * @param args - initial args.
     */
    public static void main(final String[] args) {
        JWhale.main(args);
    }
}
